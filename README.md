# Store Plan
## api resource
https://fakestoreapi.com/docs

## Technologies used
- Angular
- tailwind
- material design (not sure about in terms of being easy for dynamic content)
- rxjs 

## Running the project
- npm install  // install all npm modules required to run project
# dev server
- npm run start


# Build
-npm run build

# Live version of project

https://jacksbazzar.netlify.app/home


## Origional plan!
Build a web app with three basic components:

    List of items
    Details section
    Cart

Details of each component:

1. Item listing

Display table containing the following

    title,
    price,
    rating
    action button to add item to cart.

Add pagination to the table

 

2. Details section

    Show the details in new component when an item in the list is selected.

    image,
    title,
    description
    price,
    rating : rate and count

Add an action button at bottom to add in cart.

 

3. Cart

    Add to cart when add action button is clicked

    When cart button is clicked show a popup with list of items selected in simple text view with total amount.

 

Use the following API reference: https://fakestoreapi.com/docs



