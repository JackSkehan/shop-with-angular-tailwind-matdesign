import { Component, EventEmitter, Output } from '@angular/core';


@Component({
  selector: 'app-sort',
  templateUrl: './sort.component.html',
})
export class SortComponent {
  @Output() showCategory = new EventEmitter<string>();
  @Output() showDetailsChange = new EventEmitter<boolean>();
  @Output() itemsCountChange = new EventEmitter<number>();
  @Output() sortChange = new EventEmitter<string>();
  itemsShowCount = 12;
  sort = 'desc';

  constructor(
    
  ) {}

  productDetailShow(detailed: boolean): void {
    this.showDetailsChange.emit(detailed);
  }

  onItemsUpdated(count: number): void {
    this.itemsCountChange.emit(count);
    this.itemsShowCount = count;
  }

  onSortUpdated(newSort: string): void {
    this.sortChange.emit(newSort);
    this.sort = newSort;
  }
  onShowCategory(category: string): void {
    this.showCategory.next(category);    
  }


}
