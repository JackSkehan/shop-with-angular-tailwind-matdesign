import { Component,Input } from '@angular/core';
import { CartItem } from 'src/app/models/cart.model';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-cart-widget',
  templateUrl: './cart-widget.component.html',
})
export class CartWidgetComponent {
  private _cart: CartItem[] =  [];
  itemsQuantity = 0;


@Input()
get cart(): CartItem[]{
  return this._cart;
}

set cart(cart: CartItem[]) {
  this._cart = cart;
  this.itemsQuantity = cart
    .map((item) => item.quantity)
    .reduce((prev, curent) => prev + curent, 0);
}

constructor(
  private cartService: CartService,
  ) { }

getTotal(items: CartItem[]): number {
  return this.cartService.getTotal(items);
}


onClearCart(): void {
  this.cartService.clearCart();
}
}