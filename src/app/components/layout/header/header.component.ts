import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CartItem } from 'src/app/models/cart.model';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { SideNavService } from 'src/app/services/sideNav.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})

export class HeaderComponent {
  public darkMode: boolean | undefined;
  public darkModeKey = "darkMode";

  itemsQuantity = 0;

  @Output() darkModeToggle = new EventEmitter<boolean>();
  @Input() currentTitle: string = "Jack's Bazzar";
  @Input() cart: CartItem[] = []

  constructor(
    private localStorageService: LocalStorageService,
    private sideNavService: SideNavService,
  ) { this.darkMode = this.localStorageService.get(this.darkModeKey) }

  private emitDarkMode(darkMode: boolean): void {
    this.darkModeToggle.emit(darkMode);
  }

  public toggleDarkMode(): void {
    this.darkMode = !this.darkMode;
    this.emitDarkMode(this.darkMode);
  }

  toggleSideNav():void {
    this.sideNavService.toggle();    
 }


}




