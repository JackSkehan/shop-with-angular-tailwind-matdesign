import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from 'src/app/models/product.model';

@Component({
  selector: 'app-product-box',
  templateUrl: './product-box.component.html',
  styleUrls: ['./product-box.component.scss']
})
export class ProductBoxComponent {
  @Input() detailed = false;
  @Input() product: Product | undefined;
  @Output() addToCart = new EventEmitter();
  @Output() removeFromCart = new EventEmitter();
  public truncate: boolean = true;
  constructor(
  ) {}

  onAddToCart(): void {
    this.addToCart.emit(this.product);
  }
  onRemoveFromCart(): void {
    this.removeFromCart.emit(this.product);
  }
}
