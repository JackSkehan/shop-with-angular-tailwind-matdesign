import { Component, ViewChild, AfterViewInit, OnInit } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { CartItem } from './models/cart.model';
import { CartService } from './services/cart.service';
import { LocalStorageService } from './services/local-storage.service';
import { Subscription } from 'rxjs';
import { SideNavService } from './services/sideNav.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements  AfterViewInit,OnInit {
  public darkMode: boolean = false;
  private darkModeKey: string = "darkMode"
  cartSubscription: Subscription | undefined;
  cart: CartItem[] = [];


  @ViewChild('matDrawer') public sidenav!: MatDrawer;
  constructor(
    private cartService: CartService,
    private localStorage: LocalStorageService,
    private sideNavSevice: SideNavService,
  ) {
    this.darkMode = this.localStorage.get(this.darkModeKey)
  }

  ngOnInit() {
    if (this.cart) {
      this.cartService.cart$.subscribe((_cart) => {
        this.cart = _cart;
      });
    }
  }
  ngAfterViewInit(): void {
    this.sideNavSevice.setSidenav(this.sidenav);
  }

 public closeSideNav(){
    this.sideNavSevice.close();
 }
  public onDarkModeToggle(darkMode: boolean): void {
    this.darkMode = darkMode;
    this.localStorage.set(this.darkModeKey,darkMode)
  }

  ngOnDestroy(): void {
    if (this.cartSubscription) {
      this.cartSubscription.unsubscribe();
    }
  }
}
