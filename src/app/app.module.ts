// Angular Modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';


// Pages
import { HomeComponent } from './pages/home/home.component';

// Components
import { CartComponent } from './pages/cart/cart.component';
import { ProductBoxComponent } from './components/layout/product-box/product-box.component'; 
import { HeaderComponent } from './components/layout/header/header.component';
import { CartWidgetComponent } from './components/layout/cart-widget/cart-widget.component';
import { SortComponent } from './components/utilities/sort/sort.component';
import { FiltersComponent } from './components/utilities/filters/filters.component';

// Services
import { CartService } from './services/cart.service';
import { StoreService } from './services/store.service';
import { SideNavService } from './services/sideNav.service';
import { LocalStorageService } from './services/local-storage.service';

//Pipes
import { EuroCurrencyPipe } from './pipes/euro-currency.pipe';
import { TruncatePipe } from './pipes/truncatPipe';
// Material Modules
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTreeModule } from '@angular/material/tree';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MatBadgeModule } from '@angular/material/badge';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AboutMeComponent } from './pages/about-me/about-me.component';






@NgModule({
  declarations: [
    AppComponent,
    CartComponent,
    HomeComponent,
    HeaderComponent,
    ProductBoxComponent,
    FiltersComponent,
    SortComponent,
    CartWidgetComponent,
    EuroCurrencyPipe,
    TruncatePipe,
    AboutMeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatSidenavModule,
    MatGridListModule,
    MatMenuModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatExpansionModule,
    MatTreeModule,
    MatListModule,
    MatToolbarModule,
    MatTableModule,
    MatBadgeModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    HttpClientModule,

  ],
  providers: [ CartService, StoreService, SideNavService,LocalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
