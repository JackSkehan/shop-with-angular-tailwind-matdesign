import { Component, OnDestroy, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Product } from 'src/app/models/product.model';
import { CartService } from 'src/app/services/cart.service';
import { StoreService } from 'src/app/services/store.service';

const ROWS_HEIGHT: number = 270;
const SHOWDETAILSHEIGHT:number = 310

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit, OnDestroy {

  cols = 3;
  rowHeight: number = this.getRowHeight();
  products: Array<Product> | undefined;
  count = '12';
  sort = 'desc';

  category: string | undefined;
  productsSubscription: Subscription | undefined;
  resizeSubscription: Subscription | undefined;
  breakpoint: number | undefined;
  showDetails: boolean = false;


  
  constructor(
    private cartService: CartService,
    private storeService: StoreService,
  ) {
    const resizeDebounce = fromEvent(window, 'resize');
    this.resizeSubscription = resizeDebounce.pipe(
      debounceTime(200)
    ).subscribe((event: any) => {
      this.onResize(event);
    });
  }

  ngOnInit(): void {
    this.getProducts();
    this.breakpoint = this.breakpointToCols(window.innerWidth);
    this.rowHeight = this.getRowHeight();
  }

  onResize(window: any): void {
    this.breakpoint = this.breakpointToCols(window.target.innerWidth);
    this.rowHeight = this.getRowHeight();
    console.log("resized" + this.breakpoint);
  }

  getRowHeight():number{
    return  this.showDetails? SHOWDETAILSHEIGHT : ROWS_HEIGHT
  }

  breakpointToCols(innerWidth: number) {
    if (innerWidth < 768) {
      return 1;
  }
  else if (innerWidth >= 768 &&  innerWidth <= 992) {
    return 2;
  }
  else  {
    return 3;
  }
  }



  showDetailsChange(showDetails: boolean): void {
    this.showDetails = showDetails
    this.rowHeight = this.getRowHeight();
  }

  onItemsCountChange(count: number): void {
    this.count = count.toString();
    this.getProducts();
  }

  onSortChange(newSort: string): void {
    this.sort = newSort;
    this.getProducts();
  }

  onShowCategory(newCategory: string): void {
    this.category = newCategory;
    this.getProducts();
  }

  getProducts(): void {
    this.productsSubscription = this.storeService
      .getAllProducts(this.count, this.sort, this.category)
      .subscribe((_products) => {
        this.products = _products;
      });
  }

  onAddToCart(product: Product): void {
    this.cartService.addToCart({
      product: product.image,
      name: product.title,
      price: product.price,
      quantity: 1,
      id: product.id,
    });
  }

  onRemoveFromCart(product: Product): void {
    this.cartService.removeFromCart({
      product: product.image,
      name: product.title,
      price: product.price,
      quantity: 1,
      id: product.id,
    });
  }

  ngOnDestroy(): void {
    if (this.productsSubscription) {
      this.productsSubscription.unsubscribe();
    }
    if (this.resizeSubscription) {
      this.resizeSubscription?.unsubscribe()
    }
  }
}
