import { Component, Input } from '@angular/core';
import { CartItem } from 'src/app/models/cart.model';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent {
  cart: CartItem[] = [];
  itemsQuantity = this.cart.length;

  constructor(
    private cartService: CartService,
    ) { }

    ngOnInit() {
      if (this.cart) {
        this.cartService.cart$.subscribe((_cart) => {
          this.cart = _cart;
          this.itemsQuantity = this.cart
          .map((item) => item.quantity)
          .reduce((prev, curent) => prev + curent, 0);
        });
      }
    }
  
  removeItem(item: CartItem){
    this.itemsQuantity--
    return this.cartService.removeFromCart(item);
  }
  addItem(item: CartItem){
    this.itemsQuantity++;
    return this.cartService.addToCart(item);
    
  }

  getTotal(items: CartItem[]): number {
    return this.cartService.getTotal(items);
  }
  
  
  onClearCart(): void {
    this.cartService.clearCart();
  }
  }