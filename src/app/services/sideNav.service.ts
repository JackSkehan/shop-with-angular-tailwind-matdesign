import { Injectable } from '@angular/core';
import {MatDrawer} from '@angular/material/sidenav'; 


@Injectable()
export class SideNavService {
    private sidenav?: MatDrawer
    constructor()
    {}

    public setSidenav(sidenav: MatDrawer) {
        this.sidenav = sidenav;
    }

    public toggle(): void {
    this.sidenav?.toggle();
   }

   public open(): void {
    this.sidenav?.open();
   }

   public close(): void {
    this.sidenav?.close();
   }
}