import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  localStorage: Storage;

  changes$ = new Subject();

  constructor() {
    this.localStorage = window.localStorage;
  }

  get(key: string): any {
    if (this.isLocalStorageSupported) {
      return JSON.parse(this.localStorage.getItem(key) || '{}');
    }

    return null;
  }

  set(key: string, value: any): boolean {


    if (this.isLocalStorageSupported) {
      const jsonData = JSON.stringify(value)
      this.localStorage.setItem(key, jsonData);
      this.changes$.next({
        type: 'set',
        key,
        value
      });
      return true;
    }
    console.log("Nsupported");
    return false;
  }

  remove(key: string): boolean {
    if (this.isLocalStorageSupported) {
      this.localStorage.removeItem(key);
      this.changes$.next({
        type: 'remove',
        key
      });
      return true;
    }

    return false;
  }

  get isLocalStorageSupported(): boolean {
    if (this.localStorage) { return true; } return false;
  }
}