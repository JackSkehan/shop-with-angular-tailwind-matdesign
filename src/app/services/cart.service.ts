import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { CartItem } from '../models/cart.model';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  // Make _cartSource private so it's not accessible from the outside, 
  // expose it as cart$ observable (read-only) instead.
  // Write to _cartSource only through specified store methods below.
  private readonly _cartSource = new BehaviorSubject<CartItem[]>([]);

  // Exposed observable (read-only).
  readonly cart$ = this._cartSource.asObservable();
  private readonly cartKey: string = "cart"
  constructor(
    private _snackBar: MatSnackBar,
    private localStorageService: LocalStorageService
  ) { this._cartSource.next(JSON.parse(localStorage.getItem("cart") || "[]" ))}

  // Get last value without subscribing to the cart$ observable (synchronously).
  getCart(): CartItem[] {
    return this._cartSource.getValue();
  }

  private _setCartItems(cart: CartItem[]): void {
    this.localStorageService.set(this.cartKey, cart)
    this._cartSource.next(cart);
  }

  addToCart(item: CartItem): void {
    const items = [...this._cartSource.value];

    const itemInCart = items.find((_item) => _item.id === item.id);
    if (itemInCart) {
      itemInCart.quantity += 1;
    } else {
      items.push(item);
    }
    this._setCartItems(items);
    this._snackBar.open('1 item added to cart.', 'Ok', { duration: 3000 });

  }


  removeQuantity(item: CartItem, updateCart = true): CartItem[] {
    const filteredItems = this._cartSource.value.filter(
      (_item) => _item.id !== item.id
    );

    if (updateCart) {
      this._setCartItems(filteredItems);
      this._snackBar.open('1 item removed from cart.', 'Ok', {
        duration: 3000,
      });
    }

    return filteredItems;
  }

  removeFromCart(item: CartItem): void {
    let itemForRemoval!: CartItem;

    let filteredItems = this._cartSource.value.map((_item) => {
      if (_item.id === item.id) {
        _item.quantity--;
        if (_item.quantity === 0) {
          itemForRemoval = _item;
        }
      }

      return _item;
    });

    if (itemForRemoval) {
      filteredItems = this.removeQuantity(itemForRemoval, false);
    }

    this._setCartItems(filteredItems);
    this._snackBar.open('1 item removed from cart.', 'Ok', {
      duration: 3000,
    });
  }

  clearCart(): void {
    this._setCartItems([]);
    this._snackBar.open('Cart is cleared.', 'Ok', {
      duration: 3000,
    });
  }

  getTotal(items: CartItem[]): number {
    return items
      .map((item) => item.price * item.quantity)
      .reduce((prev, current) => prev + current, 0);
  }
}
